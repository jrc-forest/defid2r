---
title: "3. DEFID2 paper - Data harmonization and distribution"
author: "Loïc Dutrieux"
output:
  bookdown::html_document2:
    toc: false
    number_sections: yes
    fig_caption: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Data harmonization, management and distribution

```{R dataharmon, echo=FALSE, fig.cap="Visual representation of data harmonization and distribution workflow", out.width="80%", fig.align='center'}
knitr::include_graphics('figs/defid2_processing_distribution.png')
```

Figure \@ref(fig:dataharmon) presents graphically the various stages of data collection, harmonization and distribution. Various steps act as filters along the process, the first of which is the DEFID2 data collection template itself. A variety of contributors may possess data about pest related forest disturbances collected, organized and stored in multiple ways. The DEFID2 template ensures a certain level of compatibility of format and structure among the contributions. Contribution are then quality controlled and further harmonized at the JRC with eventual feedback from the original contributors. To further guarantee consistency across records, the GIS files are ingested into a Relational Database Management system with strict structure and field typing. Transforming the data to such fully structured data facilitates data management while preserving complex relations between attributes and ensuring data integrity. A simplified overview of the database structure is presented in Figure \@ref(fig:erd). The two main tables of that schema are **Event** and **Geom**  for disturbance events and geometries associated to these events. Note that the relationship between these events and their associated geometries is not necessarily one to one, meaning that an event may be associated to multiple geometries (a side effect of data contribution in multipart geometries) but also multiple events may be associated to a single geometry. The latter happens for instance when the provided geometries delineate forest management units in which several disturbance events have been reported along the years but not affecting the whole extent of the management unit at once. In total we identified four types of relations between geometries and disturbance events:

- exact polygon: The extent of the disturbance corresponds exactly to the coverage of the polygon geometry provided
- exact point: A point geometry located exactly within a disturbance patch
- substitute polygon: The disturbance(s) occurred within the provided polygon geometry but does/do not cover the entire extent of it
- substitute point: A point geometry with a loose spatial relation to the disturbance event


```{R erd, echo=FALSE, fig.cap="Simplified Entity Relation Diagram (ERD) of the DEFID2 database", out.width="80%", fig.align='center'}
knitr::include_graphics('figs/defid2-db_erd.png')
```

Figure \@ref(fig:dataharmon) also includes the data dissemination pathways. From the RDBMS managed at JRC, file database (sqlite) is extracted and geospatial vector file in geopackage format is reconstructed. Both these files are made publicly available via the JRC open data repository and users can either directly download them or access them via a dedicated software package in the R programming language. In addition to being stored on the JRC open data repository, the DEFID2 datasets will be indexed with appropriate keywords in the JRC Data Catalog (https://data.jrc.ec.europa.eu/). That step ensures that data can be efficiently discovered, allows us to provide additional background and context information around the data and establishes proper attribution for use in subsequent research work. 
The R package will be published on the Comprehensive R Archive Network (CRAN - https://cran.r-project.org/) under an open-source license and maintained by the JRC. Because data volumes are large and data structure complex, providing such package abstracts some of the complexity of handling and analyzing the data, making it accessible to a wider community and maximizing the potential for impactful derived work.  


# Data exploration

```{r, include=FALSE, echo=FALSE, warning=FALSE}
library(defid2R)
library(dplyr)
library(tidyr)
library(units)
library(stringr)
library(ggplot2)
library(sf)
library(giscoR)

library(knitr)
library(kableExtra)
# sfdf <- read_defid()

knitr::opts_chunk$set(collapse = TRUE)
options(knitr.kable.NA = '-')
# Here's a good place to perform some filesystem cleaning (e.g. tmp files, bi-products of analysis)
```




```{r contrib, echo=FALSE, message=FALSE, warning=FALSE, paged.print=TRUE, results='asis'}
df <- defid_contributions() %>%
    mutate(affiliation=str_replace_all(affiliation,"\\n", "<br>"))
df %>% kable("html", escape = FALSE,
             caption='Summary of data contributions to the first version DEFID2 initiative') %>%
  kable_styling()
```



```{r eventstats, echo=FALSE, message=FALSE, warning=FALSE}
# A dataframe of all events can be queried, with associated geometries and corresponding area in squared meters
# Note that in the case of multiple events associated to a single geometry, the geometry is duplicated. This only occurs for 'substitute polygons' and is therefore not used for computing the area summary columns
# Overlapping geometries with exact geom_event_relation are counted twice; this does not happen a lot
# TODO: Add a percentage of overlap in a additional topology table

df <- defid_disturbance_statistics()

# From this dataframe, count and area statistics can be generated. We do that in two different tables
# Count
count_df <- df %>%
    group_by(Country, geom_event_relation) %>%
    summarise(count = n(), .groups='drop') %>%
    pivot_wider(names_from = geom_event_relation, values_from = count) 

# Area
area_df <- df %>%
    filter(geom_event_relation == 'exact') %>%
    group_by(Country) %>%
    summarise(area_sum = sum(area),
              area_median = median(area),
              area_std = sd(area))
# Combined
combined_df <- full_join(count_df, area_df)

# Visualization with kable extra package
# TODO: display without unit and round
kable(combined_df, col.names = NULL, caption = 'Statistics of insect disturbance records collected in the DEFID2 database aggregated by country. Affected area statistics include only event-geometry pairs with exact relations', digits = 2) %>%
    add_header_above(c(' '=1, 'Exact'=1, 'Exact point'=1, 'Substitute point'=1, 'Substitute polygon'=1, ' '=1, ' '=1, ' '=1)) %>%
    add_header_above(c('Country'=1, 'Number of records'=4, 'Cumulated area (ha)'=1, 'Median geom area (ha)'=1, 'Standard deviation (ha)'=1))
```


```{r attributescompletness, echo=FALSE, message=FALSE, warning=FALSE, fig.cap="Attribute completeness of data contributed to the DEFID2 database", out.width="80%", fig.align='center'}
# Focus on events only

# SELECT count(distinct(event.id)) FROM event RIGHT JOIN hosts_events ON event.id = hosts_events.event_id;`

# Is_affected
# Any of the severity fields 
# At least one agent
# At least one host
# At least one symptom
# At least one trigger
# Information on sylvicultural system
# Info on sanitary intervention
# Patterns of symptoms
# Note that some events may be associated with multiple geometries (case of multipart geometry provided)
df <- defid_fields_statistics()
n <- nrow(df)
df2 <- df %>% mutate(nseverities = rowSums(!is.na(across(starts_with('severity_')))),
                     ntriggers = rowSums(!is.na(across(starts_with('trigger_')))),
                     npatterns = rowSums(!is.na(across(starts_with('pattern_'))))) %>%
    select(starts_with('n'))
ggdf <- df2 %>% summarise(nagents = sum(nagents != 0),
                          nhosts = sum(nhosts != 0),
                          nsymptoms = sum(nsymptoms != 0),
                          nseverities = sum(nseverities != 0),
                          ntriggers = sum(ntriggers != 0),
                          npatterns = sum(npatterns != 0)) %>% 
    pivot_longer(cols = everything(), names_to = 'Fields', values_to = 'count') %>%
    mutate(proportion = count / n) %>%
    arrange(Fields)
gg <- ggplot(ggdf) +
    geom_bar(aes(x = proportion, y = Fields), stat='identity') +
    scale_y_discrete(NULL,
                     labels=c('ntriggers' = 'At least one\ntrigger',
                              'nsymptoms' = 'At least one\nsymptom',
                              'nseverities' = 'At least one\nseverity field',
                              'npatterns' = 'At least one\npattern field',
                              'nhosts' = 'At least one\nhost',
                              'nagents' = 'At least one\nagent')) +
    xlab('Proportion of records') +
    theme_bw()

gg

# ggsave(gg, filename = '/home/loic/git/defid2/vignettes/figs/attribute_completeness.tif', device='tiff', dpi = 300)
# ggsave(gg, filename = '/home/loic/git/defid2/vignettes/figs/attribute_completeness.pdf', device='pdf', dpi = 300)
```

Note that given the large over-representation of the data from Czechia, statistics largely reflect the status of that particular dataset.
 
 
 
```{r freqagent, echo=FALSE, message=FALSE, warning=FALSE, fig.cap="Agent distribution in reported events. Note the use of logarithmic scale.", out.width="80%", fig.align='center'}
freq_df_agents <- defid_event_frequencies(by='agent') %>%
    dplyr::arrange(desc(freq))
groups <- seq(1, nrow(freq_df_agents))
groups[groups >= 10] <- 10
freq_df_agents$groups <- groups
freq_df_agents$name[groups == 10] <- 'others'
ggdf <- freq_df_agents %>%
    dplyr::group_by(groups) %>%
    summarise(freq = sum(freq),
              name = first(name)) %>%
    mutate(name = case_when(
        grepl("^.* sp.$", name) ~ gsub("(^.*)( sp.$)", "*\\1*\\2", name),
        name == 'others' ~ 'others',
        TRUE ~ paste0('*', name, '*')
   ))

ggdf$name <- factor(ggdf$name, levels = ggdf$name)
    
gg <- ggplot(ggdf, aes(x=freq, y=name)) +
    geom_bar(stat='identity') +
    scale_x_continuous(
        trans = "log10",
        breaks = c(10, 100, 500, 1000, 5000, 10000, 100000, 1000000),
        labels = scales::comma
    ) +
    scale_y_discrete(limits=rev) +
    ylab('Agent name') +
    xlab('Number of records') +
    theme_bw() +
    theme(axis.text.x=element_text(angle=45,hjust=1)) +
    theme(axis.text.y = ggtext::element_markdown())
gg

# ggsave(gg, filename = '/home/loic/git/defid2/vignettes/figs/event_frequency.tif', device='tiff', dpi = 300)
# ggsave(gg, filename = '/home/loic/git/defid2/vignettes/figs/event_frequency.pdf', device='pdf', dpi = 300)

```


```{r freqhost, echo=FALSE, message=FALSE, warning=FALSE, fig.cap="Host distribution in reported events. Note the use of logarithmic scale.", out.width="80%", fig.align='center'}
freq_df_hosts <- defid_event_frequencies(by='host') %>%
    dplyr::arrange(desc(freq))

groups <- seq(1, nrow(freq_df_hosts))
groups[groups >= 10] <- 10
freq_df_hosts$groups <- groups
freq_df_hosts$name[groups == 10] <- 'others'
ggdf <- freq_df_hosts %>%
    dplyr::group_by(groups) %>%
    summarise(freq = sum(freq),
              name = first(name)) %>%
    mutate(name = case_when(
        grepl("^.* sp.$", name) ~ gsub("(^.*)( sp.$)", "*\\1*\\2", name),
        name == 'others' ~ 'others',
        TRUE ~ paste0('*', name, '*')
   ))

ggdf$name <- factor(ggdf$name, levels = ggdf$name)
    
gg <- ggplot(ggdf, aes(x=freq, y=name)) +
    geom_bar(stat='identity') +
    scale_x_continuous(
        trans = "log10",
        breaks = c(10, 100, 500, 1000, 5000, 10000, 100000, 1000000),
        labels = scales::comma
    ) +
    scale_y_discrete(limits=rev) +
    ylab('Host name') +
    xlab('Number of records') +
    theme_bw() +
    theme(axis.text.x=element_text(angle=45,hjust=1)) +
    theme(axis.text.y = ggtext::element_markdown())

gg

ggsave(gg, filename = '/home/loic/git/defid2/vignettes/figs/host_freq.tif', device='tiff', dpi = 300)
ggsave(gg, filename = '/home/loic/git/defid2/vignettes/figs/host_freq.pdf', device='pdf', dpi = 300)
```


```{r stmap, echo=FALSE, message=FALSE, warning=FALSE, fig.cap="Spatio-temporal distribution of reported events. Each hexagonal bin represents a area of about 20,000 km². Note the logarithmic color gradient to allow visualization of largely unbalanced densities", out.width="80%", fig.align='center'}
# Define spatial extent
bbox_europe <- st_bbox(c(xmin=-15,xmax=45,ymin=28,ymax=72), crs = st_crs(4326)) %>%
    st_as_sfc() %>%
    st_transform(st_crs(3035))
# Get gisco data
countries <- gisco_get_nuts(year='2021', epsg='3035') %>%
    filter(LEVL_CODE == 0) %>%
    st_crop(bbox_europe)
# create grid
hex_grid <- st_make_grid(countries, cellsize = 152200, square=FALSE) %>% # hex of about 20,000 km^2
    st_sf() %>%
    st_filter(countries)
# Get defid data
defid_points <- defid_st_distrib() %>%
    st_transform(st_crs(3035))

# Build st_dataframe of count within spatial bin
hex_grid$`before 1985` <- sapply(st_intersects(hex_grid, filter(defid_points, survey_date < as.Date('1985-01-01'))), length)
hex_grid$`1985-1989` <- sapply(st_intersects(hex_grid, filter(defid_points, survey_date >= as.Date('1985-01-01'), survey_date < as.Date('1990-01-01'))), length)
hex_grid$`1990-1994` <- sapply(st_intersects(hex_grid, filter(defid_points, survey_date >= as.Date('1990-01-01'), survey_date < as.Date('1995-01-01'))), length)
hex_grid$`1995-1999` <- sapply(st_intersects(hex_grid, filter(defid_points, survey_date >= as.Date('1995-01-01'), survey_date < as.Date('2000-01-01'))), length)
hex_grid$`2000-2004` <- sapply(st_intersects(hex_grid, filter(defid_points, survey_date >= as.Date('2000-01-01'), survey_date < as.Date('2005-01-01'))), length)
hex_grid$`2005-2009` <- sapply(st_intersects(hex_grid, filter(defid_points, survey_date >= as.Date('2005-01-01'), survey_date < as.Date('2010-01-01'))), length)
hex_grid$`2010-2014` <- sapply(st_intersects(hex_grid, filter(defid_points, survey_date >= as.Date('2010-01-01'), survey_date < as.Date('2015-01-01'))), length)
hex_grid$`2015-2019` <- sapply(st_intersects(hex_grid, filter(defid_points, survey_date >= as.Date('2015-01-01'), survey_date < as.Date('2020-01-01'))), length)
hex_grid$`2020 and after` <- sapply(st_intersects(hex_grid, filter(defid_points, survey_date >= as.Date('2020-01-01'))), length)

# Reshape sf dataframe for facet grid
ggdf <- pivot_longer(data = hex_grid, cols = -geometry, names_to = 'Period', values_to = 'count')
ggdf$Period <- factor(ggdf$Period, levels = c('before 1985', '1985-1989', '1990-1994', '1995-1999', '2000-2004', '2005-2009', '2010-2014', '2015-2019', '2020 and after'))
    

gg <- ggplot(ggdf) +
    geom_sf(aes(fill = count), size=0.1, color='grey') +
    geom_sf(data=countries, fill=NA, colour='black', size=0.2) +
    facet_wrap(vars(Period)) +
    theme_bw() +
    #scale_fill_viridis_c(trans='log')
    scale_fill_gradient(name = "Number of records", trans = "log", low='white', high = 'magenta', na.value = "white", breaks=c(0,20, 400, 8000)) +
    theme(legend.position="bottom") +
    theme(strip.text.x = element_text(size = 6)) +
    theme(plot.margin=grid::unit(c(0,0,0,0), "mm")) +
    theme(axis.text=element_text(size=6))
gg

ggsave(gg, filename = '/home/loic/git/defid2/vignettes/figs/st_distrib_events.tif', device='tiff', dpi = 300)
ggsave(gg, filename = '/home/loic/git/defid2/vignettes/figs/st_distrib_events.pdf', device='pdf', dpi = 300)
```
