# defid2R

<img src="man/figures/logo.png" align="right" height="139" />

An R package to facilitate access and analysis of the Database of European Forest Insect and Disease Disturbances (DEFID2).


The DEFID2 initiative is an attempt to collect, harmonize and distribute in a standardized way data on biotic disturbances that have affected European forests. The effort is coordinated by the JRC and version 1.0 of the database received data contributions from 22 organization, covering 8 countries. The `defid2R` package facilitates data access and analysis.


[Tutorials, documentation and additional resources](#tutorials-documentation-and-additional-resources) • [Functionalities](#functionalities) • [Installing](#installing) • [Contributing](#contributing) • [How to cite](#how-to-cite)

## Tutorials, documentation and additional resources

* [Package documentation](https://jrc-forest.pages.code.europa.eu/defid2r/)
* [Database structure and data exploration](https://jrc-forest.pages.code.europa.eu/defid2r/articles/01-structure_exploration.html)
* [Extracting "DEFID2 extension" variables in Google Earth Engine](https://jrc-forest.pages.code.europa.eu/defid2r/articles/02-defid2_gee_code.html)
* Scientific article presenting the DEFID2 initiative and dataset
* [Forest and Tree Pests research at JRC](https://forest.jrc.ec.europa.eu/en/activities/forest-and-tree-pests/)
* [DEFID2 dataset](https://jeodpp.jrc.ec.europa.eu/ftp/jrc-opendata/FOREST/DISTURBANCES/DEFID2/)

## Functionalities

### Data download

When attaching the package for the first time with `library(defid2R)`, the user will be prompted to download the DEFID2 database. The database is a sqlite file of about 400 MB. Data download is only required once and on every new session the local version of the database will be checked against the latest online version; in case a newer version is available, user will be prompted to upgrade it.

### Data analysis

The main function of the package is `read_defid()`, it allows reading the data or a subset of it as a [sf dataframe](https://r-spatial.github.io/sf/). Many analyses may not require the entire dataset (which can be rather large) to be loaded in memory. Multiple filters and field selection options are available, so that the right data subset can be queried an loaded and analysed.


## Installing

The package can be installed from this repository using:

```r
devtools::install_git('https://code.europa.eu/jrc-forest/defid2r')
```

Note that it depends on `sf`, which depending on the platform and installation strategy is not always trivial to install. Therefore if installation of dependencies is not resolved automatically, refer to the [sf installation instructions](https://r-spatial.github.io/sf/#installing).

## Contributing

### I have data on biotic forest disturbances in Europe

If you want to contribute data, which may be included in the next release cycle of the DEFID2 database, send an e-mail to JRC-DEFID2@ec.europa.eu

### I found a bug in the package or I want to suggest an improvement

Open an [issue](https://code.europa.eu/jrc-forest/defid2r/-/issues/new) or send a merge request at [https://code.europa.eu/jrc-forest/defid2r](https://code.europa.eu/jrc-forest/defid2r)

## How to cite

### Research paper

**Forzieri G, Dutrieux LP, Elia A, Eckhardt B, Caudullo G, Taboada FÁ, Andriolo A, Bălacenoiu F, Bastos A, Buzatu A, Castedo Dorado F, Dobrovolný L, Duduman M, Fernandez-Carillo A, Hernández-Clemente R, Hornero A, Ionuț S, Lombardero MJ, Junttila S, Lukeš P, Marianelli L, Mas H, Mlčoušek M, Mugnai F, Nețoiu C, Nikolov C, Olenici N, Olsson P, Paoli F, Paraschiv M, Patočka Z, Pérez-Laorga E, Quero JL, Rüetschi M, Stroheker S, Nardi D, Ferenčík J, Battisti A, Hartmann H, Nistor C, Cescatti A, Beck PSA** (2023). The Database of European Forest Insect and Disease Disturbances: DEFID2. _Global Change Biology_


### R package

**Dutrieux L** (2023). defid2R: Access and analyse the Database of European Forest Insect and Disease Disturbances (DEFID2). _R package_
version 0.0.1.


