# Author(s): Loic Dutrieux (loic.dutrieux@ec.europa.eu)
#
# Copyright (C) 2023 European Union (Joint Research Centre)
#
# This file is part of defid2R
#
# defid2R is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# defid2R is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with defid2R.  If not, see <https://www.gnu.org/licenses/>.


#' Functions exported for manuscript reproducibility purpose
#' @name reproducibility
#'
#' @details
#'     \code{defid_st_distrib} produces a simplified sf dataframe to explore spatio-temporal distribution of DEFID2 contributions
#'     \code{defid_fields_statistics} produces an intermediary dataframe later used to report on attributes completeness
#'     \code{defid_disturbance_statistics} produces a dataframe with country information and disturbance area in ha for each event. Used to obtain country statistics by later aggregation
#'     \code{defid_event_frequencies} produces frequencies for each given host or agent
#'     Note that for \code{defid_st_distrib} both events (dates) and geoms may be duplicated; this is due to the many to many relation between the two tables
#'
#' @return \code{defid_st_distrib} returns an sf dataframe with POINT geometries corresponding to the centroid of the original geometries (or the geometry itself if originally POINT).
#'     All other functions return a dataframe
#' @export
defid_disturbance_statistics <- function(){
    query <- "
    SELECT
      st_area(the_geom, true) AS area,
      geom_event_relation.name AS geom_event_relation,
      country.name AS Country
    FROM
      event
    LEFT JOIN geoms_events ON event.id = geoms_events.event_id
    LEFT JOIN geom ON geoms_events.geom_id = geom.id
    LEFT JOIN country ON event.country_id = country.id
    LEFT JOIN geom_event_relation ON event.geom_event_relation_id = geom_event_relation.id;
    "
    db_path <- defid_get_db_path()
    con <- defid_get_connection(db_path, load_spatialite = TRUE)
    df <- DBI::dbGetQuery(con, query) %>%
        dplyr::tibble() %>%
        dplyr::mutate(area = area / 10000) # conversion to ha
    DBI::dbDisconnect(con)
    return(df)
}


#' @name reproducibility
#' @export
defid_fields_statistics <- function(){
    query <- "
    SELECT
    	e.id,
    	COUNT(DISTINCT(ae.agent_id)) AS nagents,
    	COUNT(DISTINCT(he.host_id)) AS nhosts,
    	COUNT(DISTINCT(se.symptom_id)) AS nsymptoms,
    	e.severity_defoliation,
    	e.severity_discoloration,
    	e.severity_mortality,
    	e.severity_dieback,
    	e.trigger_primary_id,
    	e.trigger_secondary_id,
    	e.silvicultural_system_id,
    	e.sanitary_intervention_id,
    	e.pattern_defoliation_id,
    	e.pattern_discoloration_id,
    	e.pattern_mortality_id,
    	e.pattern_dieback_id
    FROM event e
    LEFT JOIN agents_events ae ON ae.event_id = e.id
    LEFT JOIN hosts_events he ON he.event_id = e.id
    LEFT JOIN symptoms_events se ON se.event_id = e.id
    GROUP BY id;
    "
    db_path <- defid_get_db_path()
    con <- defid_get_connection(db_path, load_spatialite = FALSE)
    df <- DBI::dbGetQuery(con, query) %>%
        dplyr::tibble()
    DBI::dbDisconnect(con)
    return(df)
}

#' @name reproducibility
#' @param by character specifying the field used for aggregation (agent or host)
#' @export
defid_event_frequencies <- function(by='agent'){
    if(by == 'agent'){
        query <- "
        SELECT
            count(ae.agent_id) AS freq,
            agent.name
        FROM agents_events ae
        LEFT JOIN agent ON agent.id = ae.agent_id
        GROUP BY agent.name;
        "
    } else if(by == 'host'){
        query <- "
        SELECT
            count(he.host_id) AS freq,
            host.name
        FROM hosts_events he
        LEFT JOIN host ON host.id = he.host_id
        GROUP BY host.name;
        "
    } else {
        stop('Unknown by argument')
    }

    db_path <- defid_get_db_path()
    con <- defid_get_connection(db_path, load_spatialite = FALSE)
    df <- DBI::dbGetQuery(con, query) %>%
        dplyr::tibble()
    DBI::dbDisconnect(con)
    return(df)
}

#' @name reproducibility
#' @export
defid_st_distrib <- function(){
    query <- "
    SELECT
        st_centroid(g.the_geom) as geometry,
        date(e.survey_date) AS survey_date
    FROM event e
    LEFT JOIN geoms_events ge ON ge.event_id = e.id
    LEFT JOIN geom g ON g.id = ge.geom_id;
    "
    db_path <- defid_get_db_path()
    # Read 'raw' sf dataframe
    sfdf <- sf::st_read(db_path, query=query, quiet=TRUE)
    # Preprocess df in memory
    return(sfdf)
}
