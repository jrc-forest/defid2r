# Author(s): Loic Dutrieux (loic.dutrieux@ec.europa.eu)
#
# Copyright (C) 2023 European Union (Joint Research Centre)
#
# This file is part of defid2R
#
# defid2R is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# defid2R is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with defid2R.  If not, see <https://www.gnu.org/licenses/>.


#' Search for the DEFID2 database file locally and return its path or create a DBI connection with the database
#' @name db
#' @details If not file is found, the function raises an error
#'
#' @return Path of the sqlite file database
#' @export
#' @examples
#' library(defid2R)
#' library(DBI)
#' library(sf)
#' # Get db path
#' db_path <- defid_get_db_path()
#' defid_get_version(db_path)
#' df <- st_read(db_path, query="SELECT id, the_geom FROM geom LIMIT 10")
#'
#' # Get a simple connection without loading spatialite module
#' con <- defid_get_connection(db_path)
#' df <- dbGetQuery(con, "SELECT id, survey_date FROM event ORDER BY survey_date DESC LIMIT 10")
#' dbDisconnect(con)
#'
#' # Get a connection with spatialite loaded
#' con <- defid_get_connection(db_path, load_spatialite=TRUE)
#' dbGetQuery(con, "select count(*) from geom WHERE st_area(the_geom, true) > 100000") # Number of geometries larger than 10ha
#' dbDisconnect(con)
defid_get_db_path <- function(){
    # TODO: How to handle core and extension file
    data_dir <- rappdirs::user_data_dir('defid2')
    file_path <- file.path(data_dir, 'defid2.sqlite')
    if(!file.exists(file_path)){
        stop(sprintf("File not found in %s", data_dir))
    }
    return(file_path)
}

#' @name db
#' @param path Path to sqlite database containing DEFID2 data
#' @param load_spatialite Boolean indicating whether to load the spatialite extension after initializing the connection with the database
#' @export
defid_get_connection <- function(path, load_spatialite=FALSE){
    con <- DBI::dbConnect(RSQLite::SQLite(), path)
    if(load_spatialite){
        DBI::dbSendQuery(con, "SELECT load_extension('mod_spatialite');")
    }
    return(con)
}

#' @name db
#' @export
defid_get_version <- function(path){
    con <- defid_get_connection(path)
    query <- "SELECT version_number FROM version;"
    df <- DBI::dbGetQuery(con, query)
    DBI::dbDisconnect(con)
    # Assert version table has a single row
    stopifnot(nrow(df) == 1)
    return(df$version_number[1])
}
